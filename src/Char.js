import React from 'react'

const charComponent = ({char, click, index}) =>
  <li onClick={e => click(char, index)}>{char}</li>

export default charComponent

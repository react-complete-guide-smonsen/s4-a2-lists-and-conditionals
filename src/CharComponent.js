import React from 'react'
import Char from './Char'

const keygen = () => parseInt(Math.random() * (100 + 10000) + 100, 10);

const charComponent = props =>
  <ul>
    { props.text.split('').map((char, i) =>
        <Char key={keygen()} {...{char}} {...props}>{char}</Char>)
    }
  </ul>

export default charComponent

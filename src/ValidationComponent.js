import React from 'react'

const validationComponent = ({text}) => (text.length >= 5) ? <p>Text too long</p> : <p>Text too short</p>

export default validationComponent
